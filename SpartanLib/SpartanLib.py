#import RPB.GPIO as GPIO
sereal_data_speed = 1544

def sereal_send(data):
	if data != '':
		return data + ' : Sereal'
	
def fore_bit_bus_send(data):
	if data != '':
		return data + ' : Fore'

def eight_bit_bus_send(data):
	if data != '':
		# Do stuff
		return data + ' : Eight'

class SpartanLib:
	@staticmethod
	def send_packet(id,Data):
		switch = {0:sereal_send(Data), 1:fore_bit_bus_send(Data), 2:eight_bit_bus_send(Data)}
		return switch.get(id)
	def get_packet(id):
		return None


class SpartanHub:
	bus_array = []
	packet_buffer_size = 0

	def Setup(self):
		return 0

	def __init__(self, bus_array, packet_buffer_size):
		self.packet_buffer_size = packet_buffer_size
		self.bus_array = bus_array
		self.Setup()



	def get_packet_buffer_size():
		return self.bus_buffer_size

	def get_bus(self,id):
		for data in self.bus_array:
			if data.get_id() == id:
				return data
		return None

class SpartanBus:
	com_types = ({'Sereal': 1, '4bit': 2, '8bit': 3, 'net': 4})

	port_type = ''
	bus_id = None
	RX_buffer, TX_buffer = [], []

	def __init__(self, id, port_type):
		self.bus_id = id
		self.port_type = port_type
	def get_id(self):
		return self.bus_id
	def get_port_type(self):
		return self.port_type
	def send(self,packet):
		# Sends packet
		SpartanLib.send_packet(self.id,packet)
		return 0
	def reseve(self):
		# Gets packet
		for x in RX_buffer:
			break
		return self.RX_buffer
	